<?php
require_once "../config/db_connection.php";
try {
    $query = "INSERT INTO comments(name,body,entry_id)
        VALUES
        ('User1','SUPER 1 ENTRIES',1),
        ('User2','SUPER 2 ENTRIES',2),
        ('User3','SUPER 3 ENTRIES',3)
        ";
    $pdo->exec($query);
}catch (Exception $exception){
    echo "Fixtures error" . $exception->getCode() . ' message: ' . $exception->getMessage();
    die();
}
header('Location: ../index.php');