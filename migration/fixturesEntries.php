<?php
require_once "../config/db_connection.php";
try {

    $query = "INSERT INTO entries(title,intro,content)
        VALUES
        ('Entries1','SUPER 1 ENTRIES','Super Content here1'),
        ('Entries2','SUPER 2 ENTRIES','Super Content here2'),
        ('Entries3','SUPER 3 ENTRIES','Super Content here3')
        ";
    $pdo->exec($query);
}catch (Exception $exception){
    echo "Fixtures error" . $exception->getCode() . ' message: ' . $exception->getMessage();
    die();
}
header('Location: ../index.php');