<?php
require_once "../config/db_connection.php";
try {
    $entriesSql = "CREATE TABLE entries(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
    title VARCHAR(255) NOT NULL,
    intro VARCHAR(150) NOT NULL,
    content TEXT NOT NULL)
    DEFAULT  CHARACTER SET utf8 ENGINE=InnoDB";
    $pdo->exec($entriesSql);

    $commentsSql = "CREATE TABLE comments(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
    name VARCHAR(150) NOT NULL,
    body VARCHAR(255) NOT NULL,
    entry_id INT,
    FOREIGN KEY (entry_id) REFERENCES entries(id) ON DELETE CASCADE ON UPDATE CASCADE)
    DEFAULT  CHARACTER SET utf8 ENGINE=InnoDB";
    $pdo->exec($commentsSql);

}catch (Exception $exception){
    echo "Error creating table! " . $exception->getCode() . ' message: ' . $exception->getMessage();
    die();
}
header('Location: ../index.php');