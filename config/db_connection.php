<?php
require_once 'db.php';
try {
    $pdo = new PDO('mysql:host=' . $host . ';dbname=' . $dbName, $dbUser, $dbPassword);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->exec('SET NAMES "utf8"');
}catch (Exception $exception){
    echo "Error connection to dataBase!" . $exception->getCode() . ' message: ' . $exception->getMessage();
    die('Wasn\'t able to connect to dataBase! ');
}