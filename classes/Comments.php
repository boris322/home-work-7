<?php


class Comments
{
    protected  $id;
    protected  $name;
    protected  $content;
    protected  $entryId;


    public function __construct($name, $content, $entryId)
    {
        $this->name     = $name = htmlspecialchars($name);
        $this->content  = $content = htmlspecialchars($content);
        $this->entryId = htmlspecialchars($entryId);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function getEntryId()
    {
        return $this->entryId;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function store(PDO $pdo){
        try {
            $sql = "INSERT INTO comments SET
            name = :name,
            body = :content,
            entry_id = :entry_id";


            $statement = $pdo->prepare($sql);
            $statement->bindValue(':name', $this->getName());
            $statement->bindValue(':content', $this->getContent());
            $statement->bindValue(':entry_id', $this->getEntryId());
            $statement->execute();
        }catch (Exception $exception){
            echo 'Code: ' . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }

    static public function getCommentsByEntryId($entryId, PDO $pdo){
        try {
            $sql = "SELECT * FROM comments WHERE entry_id=:entryId";
            $statement = $pdo->prepare($sql);
            $statement->bindValue(':entryId', $entryId);
            $statement->execute();
            $commentsArr = $statement->fetchAll();

            $commentsObjs = [];
            foreach ($commentsArr as $commentArr) {
                $commentsObj = new self($commentArr['name'],$commentArr['body'],$commentArr['entry_id']);
                $commentsObj->setId($commentArr['id']);
                $commentsObjs[] = $commentsObj;
            }
            return $commentsObjs;

            
        }catch (Exception $exception){
            echo 'Code: ' . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }

}