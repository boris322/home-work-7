<?php
require_once 'DBEntity.php';
class Entries
{
    protected $id;
    protected $title;
    protected $intro;
    protected $content;


    public function __construct($title, $intro, $content)
    {
        $this->title = $title;
        $this->intro = $intro;
        $this->content = $content;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getIntro()
    {
        return $this->intro;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function store(PDO $pdo){
        try {
            $sql = 'INSERT INTO entries SET
            title = :title,
            intro = :intro,
            content = :content';

            $statement = $pdo->prepare($sql);
            $statement->bindValue(':title', $this->title);
            $statement->bindValue(':intro', $this->intro);
            $statement->bindValue(':content', $this->content);
            $statement->execute();

        }catch (Exception$exception){
            echo "Error storing entries! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }

    static public function getById($id, PDO $pdo){
        $id = htmlspecialchars($id);
        try{
            $sql = 'SELECT * FROM entries WHERE id=:id';
            $statement = $pdo->prepare($sql);
            $statement->bindValue(':id', $id);
            $statement->execute();

            $entriesArr = $statement->fetchAll();
            $entryArr = $entriesArr[0];
            $entryObj = new self($entryArr['title'], $entryArr['intro'], $entryArr['content']);
            $entryObj->setId($entryArr['id']);
            return $entryObj;
        }catch(Exception $exception){
            header('Location: 404.php');
            die();
        }
    }

    static public function all(PDO $pdo){
        try {

            $sql = 'SELECT * FROM entries';
            $statement = $pdo->query($sql);
            $entriesArr = $statement->fetchAll();
            $entriesObjs = [];
            foreach ($entriesArr as $entries) {
                $entriesObj = new self($entries['title'], $entries['intro'], $entries['content']);
                $entriesObj->setId($entries['id']);
                $entriesObjs[] = $entriesObj;
            }
            return $entriesObjs;
        }catch (Exception $exception){
            header('Location: ../404.php');
        }
    }

    static public function getLastFiveEntries(PDO $pdo){
        try{
            $sql = 'SELECT * FROM entries ORDER BY id DESC LIMIT 5';
            $statement = $pdo->query($sql);
            $entriesArr = $statement->fetchAll();
            $entriesObjs = [];
            foreach ($entriesArr as $entryArr) {
                $entryObj = new self($entryArr['title'], $entryArr['intro'], $entryArr['content']);
                $entryObj->setId($entryArr['id']);
                $entriesObjs[] = $entryObj;
            }
            return $entriesObjs;
        }catch (Exception $exception){
            echo "Error getting entries! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }

    static public function delete($id, PDO $pdo){
        $id = htmlspecialchars($id);
        $product = self::getById($id, $pdo);
        $product->destroy($pdo);
    }

    public function destroy($pdo){
        try {
            $sql = "DELETE FROM entries WHERE id=:id";
            $statement = $pdo->prepare($sql);
            $statement->bindValue(':id', $this->id);
            $statement->execute();
        }catch (Exception $exception){
            echo "Error deleting product! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }

    static public function upgrade($id, $title, $intro, $content, PDO $pdo){
        $id      =htmlspecialchars($id);
        $title   =htmlspecialchars($title);
        $intro   =htmlspecialchars($intro);
        $content =htmlspecialchars($content);

        $product = new self($title, $intro, $content);
        $product->setId($id);
        $product->update($pdo);
    }

    public function update(PDO $pdo){
        try {
            $sql = 'UPDATE entries SET
            title = :title,
            intro = :intro,
            content = :content
            WHERE id = :id';

            $statement = $pdo->prepare($sql);
            $statement->execute([
                ':title' => $this->title,
                ':intro' => $this->intro,
                ':content' => $this->content,
                ':id' => $this->id,
            ]);

        }catch (Exception $exception){
            echo "Error updating product! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }

}