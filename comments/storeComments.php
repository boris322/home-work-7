<?php
require_once '../config/db_connection.php';
require_once '../classes/Entries.php';
require_once '../classes/Comments.php';

if (!empty($_POST['name'])){
    $comment = new Comments($_POST['name'],$_POST['content'],$_POST['entryId']);
    $comment->store($pdo);
}
header('Location: ../entries/entryPage.php?articleId='. $_POST['entryId']);
die();

