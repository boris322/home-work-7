<?php
require_once '../config/db_connection.php';
require_once '../classes/Entries.php';

if (!empty($_POST['title'])){
    $entries = new Entries($_POST['title'],$_POST['intro'],$_POST['content']);
    $entries->store($pdo);
    header("Location:/entries/index.php");
}

