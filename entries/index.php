<?php
require_once '../config/db_connection.php';
require_once '../classes/Entries.php';

$entriesObjs = Entries::all($pdo);


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home Work 7</title>
    <!--Bootstrap style-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <style>
        .d-flex{margin-top: 15px;}
        .card{margin-left: 15px;}
    </style>
</head>
<body>
    <div class="container-xl">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="../">Blog</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="../">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="index.php">View all entries</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Admin
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="../entries/addEntries.php">Create entry</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="../migration/fixturesEntries.php">Fixtures entries</a>
                        <a class="dropdown-item" href="../migration/fixturesComments.php">Fixtures comments</a>
                    </div>
                </li>
        </div>
        </nav>
        <div class="d-flex justify-content-center">
            <h1>Last 5 entries</h1>
        </div>

            <div class="row row-cols-1 row-cols-md-3">
                <?php foreach ($entriesObjs as $entriesObj): ?>
                <div class="col mb-4">
                    <div class="card" style="width: 18rem;">
                        <div class="card-body">
                            <h5 class="card-title"><?= $entriesObj->getTitle(); ?></h5>
                            <p class="card-text"><i><?= $entriesObj->getIntro(); ?></i></p>
                            <a href="entryPage.php?articleId=<?= $entriesObj->getId(); ?>" class="btn btn-primary">View</a>
                            <a href="editEntry.php?articleId=<?= $entriesObj->getId(); ?>" class="btn btn-warning">Edit</a>
                            <a href="deleteEntry.php?articleId=<?= $entriesObj->getId(); ?>" class="btn btn-danger">Delete</a>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
    </div>










    <!--Bootstrap js-->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>