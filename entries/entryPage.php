<?php
require_once '../config/db_connection.php';
require_once '../classes/Entries.php';
require_once '../classes/Comments.php';

$entryObj = Entries::getById($_REQUEST['articleId'],$pdo);

$commentsObjs = Comments::getCommentsByEntryId($_REQUEST['articleId'], $pdo);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home Work 7</title>
    <!--Bootstrap style-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <style>
        .d-block {margin-top: 25px;}
    </style>
</head>
<body>
<div class="container-xl">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="index.php">Blog</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="../">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="index.php">View all entries</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Admin
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="entries/addEntries.php">Create entry</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="migration/fixturesEntries.php">Fixtures entries</a>
                        <a class="dropdown-item" href="migration/fixturesComments.php">Fixtures comments</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <div class="d-flex justify-content-center">
        <h1><?= $entryObj->getTitle(); ?></h1>
    </div>
    <hr>
    <div class="d-flex justify-content-center">
            <p><?= $entryObj->getContent(); ?></p>
    </div>

    <hr>

    <div class="d-block">
        <h3>Submit comment</h3>
        <form action="../comments/storeComments.php" method="post" >
            <div class="form-group">
                <label for="inputTitle">Enter your name:</label>
                <input name="name" type="text" class="form-control" id="inputTitle" placeholder="Enter your name">
            </div>
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Enter your comment:</label>
                <textarea name="content" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div>
            <input name="entryId" type="hidden" value="<?=$_REQUEST['articleId']?>">
            <button type="submit" class="btn btn-outline-success">Success</button>
        </form>
    </div>

    <div class="d-block">
        <div class="d-flex justify-content-start">
            <h4>Comments</h4>
        </div>
        <table class="table">
            <tbody>
            <tr>
                <td>
                    <?php foreach ($commentsObjs as $commentsObj):?>
                    <div class="alert alert-success" role="alert">
                        <h4><?= $commentsObj->getName(); ?></h4>
                        <hr>
                        <p class="text-dark"><?= $commentsObj->getContent(); ?></p>
                    </div>
                    <?php endforeach; ?>
                </td>
            </tr>
            </tbody>
    </div>



</div>




<!--Bootstrap js-->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>